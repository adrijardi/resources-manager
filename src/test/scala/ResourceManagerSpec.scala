import java.io.{PrintWriter, StringReader, ByteArrayOutputStream}

import com.jardich.ResourceManager
import org.scalatest._
import org.scalatest.matchers.ShouldMatchers

class ResourceManagerSpec extends FlatSpec with ShouldMatchers {

  "ResourceManager" should "create the right key file" in {
    val baos = new ByteArrayOutputStream()
    val fileData =
      """
        |thisisAkey=thisvalue
        |anotherkey=another value
      """.stripMargin
    new ResourceManager(new StringReader(fileData), new PrintWriter(baos)).saveKeyFile()
    baos.toString.split("\n").map(_.trim) should be ===
      """
        |anotherkey=
        |thisisAkey=
      """.stripMargin.split("\n").map(_.trim).filter(_.nonEmpty)
  }

  "ResourceManager" should "create the right sort file" in {
    val baos = new ByteArrayOutputStream()
    val fileData =
      """
        |thisisAkey=thisvalue
        |anotherkey=another value
      """.stripMargin
    new ResourceManager(new StringReader(fileData), new PrintWriter(baos)).sort()
    baos.toString.split("\n").map(_.trim) should be ===
      """
        |anotherkey=another value
        |thisisAkey=thisvalue
      """.stripMargin.split("\n").map(_.trim).filter(_.nonEmpty)
  }

  "ResourceManager" should "create the right merge file" in {
    val baos = new ByteArrayOutputStream()
    val keyFile =
      """
        |thisisAkey=thisvalue
        |anotherkey=another value
      """.stripMargin
    val dataFile =
      """
        |thisisAkey=newValue
        |dataonlyKey=its value
      """.stripMargin

    new ResourceManager(new StringReader(keyFile), new PrintWriter(baos)).mergeWithData(new StringReader(dataFile))
    baos.toString.split("\n").map(_.trim) should be ===
      """
        |anotherkey=another value
        |dataonlyKey=its value
        |thisisAkey=newValue
      """.stripMargin.split("\n").map(_.trim).filter(_.nonEmpty)
  }
}
