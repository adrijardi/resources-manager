package com.jardich

import java.io._
import java.util.Properties

import scala.collection.immutable.TreeMap

class ResourceManager(reader: Reader, writer: PrintWriter) {

  def saveKeyFile() = {
    val data = TreeMap(readFile(reader).toSeq:_*)
    writeFile(data.map( _._1 -> "" ), writer)
  }

  def sort() = {
    val data = TreeMap(readFile(reader).toSeq:_*)
    writeFile(data, writer)
  }

  def mergeWithData(dataFile: String): Unit = {
    mergeWithData(new BufferedReader(new FileReader(dataFile)))
  }

  def mergeWithData(dataReader: Reader): Unit = {
    val keys = readFile(reader)
    val data = readFile(dataReader)

    val merged = (keys.keySet ++ data.keySet).toList.sorted.map { key ⇒
      key -> data.get(key).orElse(keys.get(key)).getOrElse("")
    }
    writeFile(TreeMap(merged:_*), writer)
  }

  def readFile(reader: Reader): Map[String, String] = {
    val prop = new Properties
    prop.load(reader)
    import scala.collection.JavaConverters._
    reader.close()
    prop.asScala.toMap
  }

  def writeFile(data: Map[String, String], writer: PrintWriter) = {
    data.foreach { case (key, value) ⇒
      writer.println(s"$key=$value")
    }
    writer.close()
  }
}

object ResourceManager {
  def apply(filename: String, outputFile: String): ResourceManager = {
    ResourceManager(reader(filename), new PrintWriter(new BufferedWriter(new FileWriter(outputFile))))
  }

  def apply(reader: Reader, writer: PrintWriter): ResourceManager = {
    new ResourceManager(reader, writer)
  }

  def reader(filename: String) = new BufferedReader(new FileReader(filename))
  def writer(filename: String) = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8")))
}
