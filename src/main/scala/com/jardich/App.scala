package com.jardich

import java.io._

object App {
  def main(args: Array[String]): Unit = {
    args match {
      case Array("keys", file, output, _*) ⇒ new ResourceManager(ResourceManager.reader(file), fileWriter(output)).saveKeyFile()
      case Array("sort", file, output, _*) ⇒ new ResourceManager(ResourceManager.reader(file), fileWriter(output)).sort()
      case Array("merge", keysFile, dataFile, output, _*) ⇒ new ResourceManager(ResourceManager.reader(keysFile), fileWriter(output)).mergeWithData(dataFile)
      case _ ⇒ println(
        """
          |Usage:
          |keys <file> <outputFile> : Will get an output file with only the key part
          |sort <file> <outputFile> : Sorts the lines by the key
          |merge <keysFile> <dataFile> <outputFile> : Will get the keys file and fill it with the values of the dataFile plus the entries on the dataFile that were missing on the keysFile
        """.stripMargin)
    }
  }

  def fileWriter(file: String) = {
    if(new File(file).exists())
      throw new IllegalArgumentException("File already exists")
    ResourceManager.writer(file)
  }

}
