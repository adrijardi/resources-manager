# Performs different operations over resource files
This project allows to perform multiple operations over key value files (property files) with the format key=value

## Dependencies

- Scala
- sbt-assembly plugin

## Operations

- Get only keys
- Sort entries by key
- Merge two files getting the keys from both but prioritizing values from one of them

## Compile
```
#!scala

sbt assembly
```

## Execute
```
#!cmd

java -jar target\scala-2.11\resources-manager-assembly-1.0.jar
```